using System;

class Program
{
   private static int _rooms;
   static int Rooms {get {return _rooms;} set {if (_rooms > 10) _rooms = 0; else _rooms = value;}}

   static void Main()
   {
      Console.Write("How many rooms will be rented? ");
      Rooms = int.Parse(Console.ReadLine());

      Person[] people = new Person[10];
      for (int i = 0; i < Rooms; i++)
      {
         string tmpName = "";
         string tmpEmail = "";
         int tmpRentedRoom = 0;

         Console.WriteLine($"Rent #{i +1}");
         Console.Write("Name: "); tmpName = Console.ReadLine();
         Console.Write("Email: "); tmpEmail = Console.ReadLine();
         Console.Write("Room: "); tmpRentedRoom = int.Parse(Console.ReadLine());

         people[tmpRentedRoom] = new Person();
         people[tmpRentedRoom].name = tmpName;
         people[tmpRentedRoom].email = tmpEmail;
         people[tmpRentedRoom].rentedRoom = tmpRentedRoom;

         Console.WriteLine();
      }

      Console.WriteLine("Busy rooms:");
      for (int i = 0; i < people.Length; i++)
      {
         if (people[i] != null) {
            Console.WriteLine($"{people[i].rentedRoom}: {people[i].name}, {people[i].email}");
         }
      }
   }
}

class Person
{
   public string name;
   public string email;
   public int rentedRoom;
}